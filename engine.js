/*
* Copyright (C) 2015 Bryan Learn
* License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
*
* File: engine.js
* Purpose: Provides a stable game loop with user-defined functions for initialization, game updates, and rendering
*/

var requestAnimFrame = window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame    ||
  window.oRequestAnimationFrame      ||
  window.msRequestAnimationFrame     ||
  function(callback){ window.setTimeout(callback, 100) };

// GameLoop
var GameLoop = function (i,d,u,e){

  var running = false;
  var isPause = true;

  var nextTime = (new Date).getTime(); // time in milliseconds
  var currTime = null;
  var delta = 1000;
  var maxTimeDiff = delta * 3; // how "in sync" update & draw are
  var skippedFrames = 1; // counter for # of consecutive skipped frames
  var maxSkippedFrames = 5; // how many frames can be skipped

  /* abstract functions */
  this.init = i;
  this.draw = d; 
  this.update = u;
  this.end = e;

  /* public functions */

  this.start = function () {
    this.init();
    run();
  }.bind(this);

  this.onpause = null;
  this.offpause = null;

  this.stop = function () {
    this.end();
    shutdown();
  }.bind(this);

  var loop = function() {
    if( !isPause ) {
        currTime = (new Date).getTime(); // time in milliseconds

        //console.log('[Run Loop] cur: '+currTime+' next: '+nextTime);

        if( (currTime - nextTime) > maxTimeDiff ){ nextTime = currTime } // recently rendered a frame?
        if( currTime >= nextTime ) { // is it time to update?
          // set next update time
          nextTime += delta;

          // update logic
          this.update();
          
          // render
          if( (currTime < nextTime) || (skippedFrames > maxSkippedFrames) ) { // Do we have enough time? Have we skipped too many frames?
              this.draw();
              skippedFrames = 1; // reset the counter
          }
          else {
            skippedFrames++; // we skipped a frame
          }
        }
        else {
          // sleep for .. ?
          var sleepTime = nextTime - currTime;

          if( sleepTime > 0 ){
            // sleep
            wait(sleepTime);
          }

        }
      } // end if(!pause)

      if(running){ requestAnimFrame(loop); }
      else { shutdown(); }
  }.bind(this);

  // param delta: time step in ms (17: ~60fps; 34: ~30fps; 67: ~15fps)
  var run = function () {
    running = true;
    requestAnimFrame(loop);
  }.bind(this); // end of Run()


  var shutdown = function () {
    running = false; 
  }.bind(this);

  /* getters */
  this.isRunning = function () { return running; };
  this.isPaused = function () { return isPause; };

  /* setters */
  this.setDelta = function (dt) { delta = dt; };

  // Busy loop that runs for ms milliseconds
  var wait = function (ms) {
    var date = new Date();
    date.setTime(date.getTime() + ms);
    while( new Date().getTime() < date.getTime()); // do nothing
    return true;
  };

  this.unpause = function () {
    isPause = false;
    if(this.offpause != null){ this.offpause(); }
  }.bind(this);

  this.pause = function () {
    isPause = true;
    if(this.onpause != null){ this.onpause(); }
  }.bind(this);

}; // end GameLoop class
