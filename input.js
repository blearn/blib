//TODO create dict of keycode mappings
/* Controller */
var Controller = function () {
  var isDown = new Object();

  /* public vars */
  // default WASD setup
  this.up = 87; // w
  this.down = 83; // s
  this.left = 65; // a
  this.right = 68; // d
  this.btn1 = 32; // Space
  this.btn2 = null;
  this.btn3 = null;
  this.btn4 = null;
  this.btn5 = null;
  this.btn6 = null;
  this.btn7 = null;
  this.btn8 = null;
  this.start = 13; // Enter

  // returns true if key is down
  this.isKeyDown = function(code) {
    if(isDown[code] != undefined){
      return isDown[code];
    }
    else { return false; }
  };

  // set key up/down flag
  this.setKey = function(code, bool) {
    isDown[code] = bool;
  };
}; // end Controller classes

/* Mouse */
var Mouse = function () {
  /* private vars */
  var btn1 = false;
  var btn2 = false;
  var mX = 0;
  var mY = 0;

  /* public vars */
  this.btn1Down = function() {
    return btn1;
  };
  this.btn2Down = function() {
    return btn2;
  };
  this.x = function() {
    return mX;
  };
  this.y = function() {
    return mY;
  };

  this.setBtn1 = function(bool) { btn1 = bool; };
  this.setBtn2 = function(bool) { btn2 = bool; };
  this.setLoc = function(x,y) {
    mX = x;
    mY = y;
  };

};
