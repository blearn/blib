blib
====
HTML5 game framework for web.

Components
==========

engine.js: Provides a stable game loop with user-defined functions for initialization, game updates, and rendering

input.js: Handles all user input (keyboard and controllers)

math.js: Contains commonly used math functions and concepts

tools.js: Contains algorithms and data structures useful for games

TODO
====
Too much
