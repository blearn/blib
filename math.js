/*
 * Copyright (C) 2015 Bryan Learn
 * License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 *
 * File: math.js
 * Purpose: Holds any common math functions or objects.
 */

function calc_dist(srcX, srcY, destX, destY){
  var dx = srcX-destX;
  var dy = srcY-destY;
  return Math.sqrt(dx*dx + dy*dy);
}

var PVector = function(aX, aY) {
  var x = aX;
  var y = aY;

  this.getX = function() { return x; };
  this.getY = function() { return y; };

  this.add = function(vectB) {
    x = x + vectB.getX();
    y = y + vectB.getY();
  }.bind(this);

  this.sub = function(vectB) {
    x = x - vectB.getX();
    y = y - vectB.getY();
  }.bind(this);

  this.mult = function(n){
    x = x*n;
    y = y*n;
  }.bind(this);

  this.div = function(n) {
    x = x/n;
    y = y/n;
  }.bind(this);

  this.mag = function() {
    return Math.sqrt(x*x + y*y);
  }.bind(this);

  this.normalize = function() {
    mag = Math.sqrt(x*x + y*y);
    x = x/mag;
    y = y/mag;
  }.bind(this);

  this.copy = function(vectB) {
    x = vectB.getX();
    y = vectB.getY();
  }.bind(this);

  this.randErr = function(err) {
    x *= 1+(err*Math.random());
    y *= 1+(err*Math.random());
  };
};
