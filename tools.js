/*
 * Copyright (C) 2015 Bryan Learn
 * License: http://www.gnu.org/licenses/gpl.html GPL version 2 or higher
 *
 * File: tools.js
 * Purpose: Holds algorithms and data structures useful in games
 */

// TODO: add collision detection

/* Circular Buffer */
var CircularBuff = function(size) {
  var max = size;
  var buff = new Array(max);
  var next = 0;

  this.add = function(element) {
    buff[next] = element;
    this.step();
  }.bind(this);

  this.step = function() {
    next++;
    if(next >= max){
      next = 0;
    }
  };

  this.nextElem = function() { return buff[next]; };
  this.nextIndx = function() { return next; };
  this.getElem = function(val) { return buff[val]; };
  this.size = function() { return max; };
};
